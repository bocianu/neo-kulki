program kulki;
uses crt,neo6502,neo6502math;

type TConfig = record
    hiscores: array [0..2] of cardinal;
    nextMode: byte;
    level: byte;
end;

const 
    BALLSIZE = 20;
    CELLSIZE = 26;
    BOARDSIZE = CELLSIZE * 9;
    BOARD_XOFFSET = 80;
    BOARD_YOFFSET = 2;
    COL_BOARD = 9;
    COL_GRID = 4;
    levelNames: array [0..2] of string[6] = ('Easy  ','Medium','Hard  ');
    nextModeNames: array [0..1] of string[9] = ('Hide Next','Show Next');
    LEVEL_BALL_POOLS: array [0..2] of byte = (5,7,9);
    BALL_MOVEMENT_DELAY = 3;
    DIR_RIGHT = 1;
    DIR_DOWN = 2;
    DIR_LEFT = 3;
    DIR_UP = 4;
    NONE = $ff;
    CONFIG_FILE = '.kulki.cnf';
    BCOLORS: array[0..8] of byte = (1,2,3,5,6,$a,$b,$c,$d);
    PALETTE: array[0..79] of byte = (
        0,$20,$20,$20,  COL_GRID,$40,$40,$30,
        BCOLORS[0],$F0,$10,$00,     BCOLORS[1],$10,$F0,$00,     BCOLORS[2],$F0,$E0,$00,
        BCOLORS[3],$E0,$00,$C0,     BCOLORS[4],$10,$60,$E0,     BCOLORS[5],$20,$60,$00,
        BCOLORS[6],$D0,$70,$00,     BCOLORS[7],$50,$20,$80,     BCOLORS[8],$00,$E0,$E0,
        BCOLORS[0]+$10,$F0,$50,$50, BCOLORS[1]+$10,$80,$F0,$80, BCOLORS[2]+$10,$F0,$f0,$80,
        BCOLORS[3]+$10,$f0,$30,$E0, BCOLORS[4]+$10,$40,$80,$F0, BCOLORS[5]+$10,$40,$80,$30,
        BCOLORS[6]+$10,$E0,$90,$30, BCOLORS[7]+$10,$50,$40,$A0, BCOLORS[8]+$10,$80,$F0,$F0
    );

var 
    board,tmpboard: array[0..8,0..8] of byte;
    _board: array[0..80] of byte absolute board;
    _tmpboard: array[0..80] of byte absolute tmpboard;
    next: array[0..2] of byte;
    mouseX,cellX: word;
    mouseY,cellY,mouseBut,mouseButChange,mouseButPrev: byte;
    boardX,boardY,ballcount,frame: byte;
    selected,target,strikedballs: byte;
    score: cardinal;
    config: TConfig;
    tmpstr: TString;
    start,quit: boolean;
    key,prevkey: char;

    ///////////////////////////////////
    ////////////////////////////////////////////  HELPERS
    ///////////////////////////////////

procedure SaveConfig;
begin
    NeoSave(CONFIG_FILE,word(@config),sizeOf(config));
end;

procedure SetPalette;
var p: byte;
begin
    NeoResetPalette;
    p := 0;
    repeat
        NeoSetPalette(PALETTE[p],PALETTE[p+1],PALETTE[p+2],PALETTE[p+3]);
        Inc(p,4);
    until p=sizeOf(PALETTE);
end;

function CountBonus(balls:byte): byte;
begin
    result := balls * (1 shl config.level);
    if config.nextMode = 1 then result := (result * 2) div 3;
end;

procedure getCellXY(x,y:byte);
begin
    cellX := x * CELLSIZE + BOARD_XOFFSET + 3;
    cellY := y * CELLSIZE + BOARD_YOFFSET + 3;
end;

///////////////////////////////////
////////////////////////////////////////////   GUI  
///////////////////////////////////

procedure DrawBoard;
var x: byte;
    w: word;
begin
    NeoSetSolidFlag(1);
    NeoSetColor(8);
    NeoDrawRect(BOARD_XOFFSET+1,BOARD_YOFFSET+1,BOARD_XOFFSET+BOARDSIZE+1,BOARD_YOFFSET+BOARDSIZE+1);
    NeoSetColor(COL_BOARD);
    NeoDrawRect(BOARD_XOFFSET,BOARD_YOFFSET,BOARD_XOFFSET+BOARDSIZE,BOARD_YOFFSET+BOARDSIZE);
    NeoSetColor(COL_GRID);
    w := 0;
    for x := 0 to 9 do begin
        NeoDrawLine(BOARD_XOFFSET+w,BOARD_YOFFSET,BOARD_XOFFSET+w,BOARD_YOFFSET+BOARDSIZE+1);
        NeoDrawLine(BOARD_XOFFSET,BOARD_YOFFSET+w,BOARD_XOFFSET+BOARDSIZE+1,BOARD_YOFFSET+w);
        w := w+CELLSIZE;
    end;
end;

procedure DrawBall(x,y:word;c:byte);
begin
    getCellXY(x,y);
    NeoSetSolidFlag(1);
    NeoSetColor(BCOLORS[c]);
    NeoDrawEllipse(cellX,cellY,cellX+BALLSIZE+1,cellY+BALLSIZE);
    NeoSetColor(BCOLORS[c]+$10);
    NeoDrawEllipse(cellX+3,cellY+2,cellX+BALLSIZE-2,cellY+BALLSIZE-6);
    NeoSetColor(8);
    NeoSetSolidFlag(0);
    NeoDrawEllipse(cellX,cellY,cellX+BALLSIZE+1,cellY+BALLSIZE);
end;

procedure ClearBall(x,y:word);
begin
    GetCellXY(x,y);
    NeoSetSolidFlag(1);
    NeoSetColor(COL_BOARD);
    NeoDrawRect(cellX-2,cellY-2,cellX+CELLSIZE-4,cellY+CELLSIZE-4);
end;

procedure PopBall(p:byte);
var x: word;
    y,s,c: byte;
begin
    c := _board[p];
    x := p mod 9;
    y := p div 9;
    GetCellXY(x,y);
    cellX := cellX + 9;
    cellY := cellY + 9;
    s := 2;
    NeoSetColor(BCOLORS[c]);
    NeoSetSolidFlag(1);
    repeat
        NeoWaitForVblank;
        NeoDrawEllipse(cellX,cellY,cellX+s,cellY+s);
        Inc(s,2);
        Dec(cellX);
        Dec(cellY);
    until s>BALLSIZE;
    ClearBall(x,y);
    DrawBall(x,y,c);
end;

procedure PopOut;
var c,s,x: byte;
begin
    s := BALLSIZE;
    x := 0;
    NeoSetSolidFlag(1);
    repeat
        NeoWaitForVblank;
        for c:=0 to 80 do begin
            if _tmpboard[c] <> _board[c] then begin
                GetCellXY(c mod 9, c div 9);
                NeoSetColor(COL_BOARD);
                NeoDrawRect(cellX,cellY,cellX+BALLSIZE+1,cellY+BALLSIZE);
                NeoSetColor(BCOLORS[_board[c]]);
                NeoDrawEllipse(cellX+x,cellY+x,cellX+x+s,cellY+x+s);
            end;
        end;
        dec(s,2);
        inc(x);
    until s=0;
    Move(tmpboard,board,81);
end;

procedure DrawSelection(x,y:word);
begin
    getCellXY(x,y);
    NeoSetSolidFlag(0);
    NeoSetColor($f);
    NeoDrawEllipse(cellX,cellY,cellX+BALLSIZE+1,cellY+BALLSIZE);
    NeoSetColor($0);
    NeoDrawEllipse(cellX+1,cellY+1,cellX+BALLSIZE-1,cellY+BALLSIZE-1);
end;

procedure DrawBalls;
var x,y: byte;
begin
    ballcount := 0;
    for y:=0 to 8 do
        for x:=0 to 8 do
            if board[y,x] <> NONE then begin
                DrawBall(x,y,board[y,x]);
                inc(ballcount);
            end else ClearBall(x,y);
end;

procedure OutlineWriteTmpStr(x,y,c0,c1,w:byte);
begin
    NeoSetColor(c0);
    NeoDrawString(x-w,y-w,tmpstr);
    NeoDrawString(x,y-w,tmpstr);
    NeoDrawString(x+w,y-w,tmpstr);
    NeoDrawString(x-w,y,tmpstr);
    NeoDrawString(x+w,y,tmpstr);
    NeoDrawString(x-w,y+w,tmpstr);
    NeoDrawString(x,y+w,tmpstr);
    NeoDrawString(x+w,y+w,tmpstr);
    NeoSetColor(c1);
    NeoDrawString(x,y,tmpstr);
end;

procedure DrawScore;
begin
    NeoSetColor(15);
    NeoSetSolidFlag(1);
    tmpstr := 'Score:';
    NeoDrawString(3,4,tmpstr);
    NeoStr(score,tmpstr);
    NeoDrawString(3,16,tmpstr);
end;

procedure DrawHiScore;
begin
    NeoSetSolidFlag(1);
    NeoSetColor(0);
    NeoDrawRect(3,46,BOARD_XOFFSET-2,54);
    NeoSetColor(15);
    tmpstr := 'HiScore:';
    NeoDrawString(3,34,tmpstr);
    NeoStr(config.hiscores[config.level],tmpstr);
    NeoDrawString(3,46,tmpstr);
end;

procedure DrawLevels;
begin
    NeoSetColor(15);
    NeoSetSolidFlag(1);
    tmpstr := 'Colors:';
    NeoDrawString(3,64,tmpstr);
    NeoStr(LEVEL_BALL_POOLS[config.level],tmpstr);
    NeoDrawString(46,64,tmpstr);
    if start then NeoSetColor(COL_GRID) else NeoSetColor(3);
    tmpstr := 'Level: ';
    NeoDrawString(3,74,tmpstr);
    NeoDrawString(40,74,levelNames[config.level]);
    NeoDrawString(3,94,nextModeNames[config.nextMode]);
end;

procedure DrawCredits;
begin
    NeoSetDrawSize(2);
    NeoSetSolidFlag(0);
    tmpstr := 'KULKI';
    OutlineWriteTmpStr(9,114,8,COL_BOARD,2);
    NeoSetColor(COL_BOARD);
    NeoSetDrawSize(1);
    tmpstr := 'neo version';
    NeoDrawString(5,132,tmpstr);
    tmpstr := 'by bocianu';
    NeoDrawString(8,142,tmpstr);
end;

procedure DrawStart;
var off: byte;
begin
    inc(frame);
    off := (frame shr 4) and 1;
    NeoSetSolidFlag(1);
    NeoSetDrawSize(2);
    NeoSetColor(0);
    NeoDrawRect(10,160,70,200);
    NeoSetSolidFlag(0);
    tmpstr := 'START';
    OutlineWriteTmpStr(10,162+off,8,3,2);
    tmpstr := 'GAME';
    OutlineWriteTmpStr(14,182+off,8,3,2);
    NeoSetDrawSize(1);
end;

procedure DrawNext;
var x,b: byte;
begin
    NeoSetColor(15);
    tmpstr := 'Next:';
    NeoDrawString(3,206,tmpstr);
    x := 6;
    b := 0;
    repeat
        NeoSetSolidFlag(1);
        if config.nextMode = 0 then NeoSetColor(8) else NeoSetColor(BCOLORS[next[b]]);
        NeoDrawEllipse(x,218,x+BALLSIZE-6,218+BALLSIZE-6);
        NeoSetSolidFlag(0);
        NeoSetColor(15);
        NeoDrawEllipse(x,218,x+BALLSIZE-6,218+BALLSIZE-6);
        inc(x,BALLSIZE+5);
        Inc(b);
    until b=3;
end;

procedure DrawScreen;
begin
    DrawBoard;
    DrawBalls;
    DrawScore;
    DrawHiScore;
    DrawLevels;
    DrawCredits;
    DrawNext;
end;

procedure ShowGameOver;
begin
    NeoSetSolidFlag(0);
    NeoSetDrawSize(8);
    tmpstr := 'GAME';
    OutlineWriteTmpStr(BOARD_XOFFSET+26,44,7,8,2);
    tmpstr := 'OVER';
    OutlineWriteTmpStr(BOARD_XOFFSET+26,140,7,8,2);
end;

///////////////////////////////////
////////////////////////////////////////////   BOARD OPERATIONS
///////////////////////////////////

procedure ClearBoard;
begin
    FillByte(@board,9*9,NONE);
    ballcount := 0;
end;

function GetRandomCell: byte;
begin
    repeat
        result := NeoIntRandom(81)
    until _board[result] = NONE;
end;

procedure DropRandomBalls(count:byte);
var c: byte;
begin
    while count>0 do begin
        c := GetRandomCell;
        _board[c] := NeoIntRandom(LEVEL_BALL_POOLS[config.level]);
        PopBall(c);
        Dec(count);
    end;
end;

procedure TossNext;
begin
    next[0] := NeoIntRandom(LEVEL_BALL_POOLS[config.level]);
    next[1] := NeoIntRandom(LEVEL_BALL_POOLS[config.level]);
    next[2] := NeoIntRandom(LEVEL_BALL_POOLS[config.level]);
end;

procedure PopulateNext;
var b,c: byte;
begin
    b := 0;
    while (ballcount<81) and (b<3) do begin
        c := GetRandomCell;
        _board[c] := next[b];
        PopBall(c);
        inc(b);
        inc(ballcount);
    end;
    TossNext;
end;

function FindPath(startpos,endpos:byte): boolean;
var 
    findQueue: array [0..81] of byte;
    b,x,y,queueSize: byte;
    found: boolean;

procedure qPush(p:byte);
begin
    findQueue[queueSize] := p;
    inc(queueSize);
end;

function qPull: byte;
begin
    result := findQueue[0];
    dec(queueSize);
    move(@findQueue[1],@findQueue[0],queueSize);
end;

procedure ProcessDir(n,dir:byte);
begin
    if (_board[n]=NONE) and (_tmpboard[n]=0) then begin
        _tmpboard[n] := dir;
        qPush(n);
    end;
end;

procedure ProcessNode(p:byte);
begin
    if p = startpos then found := true
    else begin
        x := p mod 9;
        y := p div 9;
        if (x < 8) then ProcessDir(p+1,DIR_LEFT);
        if (y < 8) then ProcessDir(p+9,DIR_UP); 
        if (x > 0) then ProcessDir(p-1,DIR_RIGHT);
        if (y > 0)  then ProcessDir(p-9,DIR_DOWN); 
    end;
end;

begin
    b := _board[startpos];
    _board[startpos] := NONE;
    FillByte(tmpboard,81,0);
    found := false;
    queueSize := 0;
    qPush(endpos);
    repeat
        ProcessNode(qPull);
    until (queueSize=0) or found;
    result := found;
    _board[startpos] := b;
end;

function IsBoardClicked: boolean;
begin
    result := (mouseButChange and mouseBut = 1)
        and (mouseX>BOARD_XOFFSET)
        and (mouseX<BOARDSIZE+BOARD_XOFFSET)
        and (mouseY>BOARD_YOFFSET)
        and (mouseY<BOARDSIZE+BOARD_YOFFSET);
    if result then begin
        boardX := (mouseX-BOARD_XOFFSET) div CELLSIZE;
        boardY := (mouseY-BOARD_YOFFSET) div CELLSIZE;
    end;
end;

procedure MoveBall(src,dest:byte);
var b,dir,d: byte;
begin
    b := _board[src];
    _board[src] := NONE;
    while src<>dest do begin
        NeoWaitForVblank;
        ClearBall(src mod 9, src div 9);
        dir := _tmpboard[src];
        case dir of 
            DIR_LEFT: src := src-1;
            DIR_RIGHT: src := src+1;
            DIR_UP: src := src-9;
            DIR_DOWN: src := src+9;
        end;
        DrawBall(src mod 9, src div 9,b);
        for d:=0 to BALL_MOVEMENT_DELAY do NeoWaitForVblank;
    end;
    _board[dest] := b;
end;

function CheckForLines: byte;
var 
    x,y,xd,yd,xxd,xx,hcount,vcount,rcount,lcount,hprev,vprev,rprev,lprev: byte;

procedure RemoveLine(x,y,xcount,ycount:byte);
begin
    if ycount=0 then Inc(result,xcount)
    else Inc(result,ycount);
    repeat
        tmpboard[y-ycount,x-xcount] := NONE;
        if xcount<>0 then if xcount<120 then dec(xcount) else inc(xcount);
        if ycount<>0 then if ycount<120 then dec(ycount) else inc(ycount);
    until (ycount=0) and (xcount=0);
end;

begin
    result := 0;
    Move(board,tmpboard,81);
    for y:=0 to 8 do begin
        hcount := 1;
        hprev := NONE;
        vcount := 1;
        vprev := NONE;
        for x:=0 to 8 do begin
            // check hlines
            if hprev = board[y,x] then inc(hcount)
            else
                begin
                    if (hcount > 4) and (hprev<>NONE) then RemoveLine(x,y,hcount,0);
                    hcount := 1;
                end;
            hprev := board[y,x];
            // check vlines
            if vprev = board[x,y] then inc(vcount)
            else
                begin
                    if (vcount > 4) and (vprev<>NONE) then RemoveLine(y,x,0,vcount);
                    vcount := 1;
                end;
            vprev := board[x,y];
        end;
        if (hcount > 4) and (hprev<>NONE) then RemoveLine(x,y,hcount,0);
        if (vcount > 4) and (vprev<>NONE) then RemoveLine(y,x,0,vcount);

        if y<5 then begin // diagonals
            if y>0 then xx := 0 else xx := 4;
            for x:=0 to xx do begin
                rcount := 1;
                rprev := NONE;
                lcount := 1;
                lprev := NONE;
                yd := y;
                xxd := 8-x;
                for xd:=x to 8-y do begin
                    // check right diagonal lines
                    if rprev = board[yd,xd] then inc(rcount)
                    else begin
                        if (rcount > 4) and (rprev<>NONE) then RemoveLine(xd,yd,rcount,rcount);
                        rcount := 1;
                    end;
                    rprev := board[yd,xd];

                    // check left diagonal lines
                    if lprev = board[yd,xxd] then inc(lcount)
                    else begin
                        if (lcount > 4) and (lprev<>NONE) then RemoveLine(xxd,yd,-lcount,lcount);
                        lcount := 1;
                    end;
                    lprev := board[yd,xxd];

                    Inc(yd);
                    Dec(xxd);
                end;
                if (rcount > 4) and (rprev<>NONE) then RemoveLine(xd,yd,rcount,rcount);
                if (lcount > 4) and (lprev<>NONE) then RemoveLine(xxd,yd,-lcount,lcount);
            end;
        end;
    end;
    if result>0 then PopOut;
end;

procedure FindStrikes;
begin
    strikedballs := CheckForLines;
    score := score + CountBonus(strikedballs);
    if strikedballs>0 then DrawScore;
end;

/////////////////////////////////////////////////   INPUT ROUTINES

procedure ReadInput;
begin
    key := #0;
    NeoReadMouse;
    mouseX := neoMouseX;
    mouseY := neoMouseY;
    mouseBut := neoMouseButtons;
    mouseButChange := mouseBut xor mouseButPrev;
    mouseButPrev := mouseBut;
    if KeyPressed then begin
        key := ReadKey;
        quit := (key=#27) and (prevkey=#27);
        prevkey := key;
    end;
end;

procedure ChangeLevel;
begin
    config.level := (config.level + 1) mod 3;
    DrawLevels;
    DrawHiScore;
    SaveConfig;
end;

procedure ChangeNextMode;
begin
    config.nextMode := config.nextMode xor 1;
    DrawLevels;
    DrawNext;
    SaveConfig;
end;

function SelectBall(x,y:byte): byte;
begin
    result := NONE;
    if board[y,x] <> NONE then begin
        result := y*9+x;
        DrawSelection(x,y);
    end;
end;

function SelectTarget(x,y:byte): byte;
var clicked: byte;
begin
    clicked := y*9+x;
    if clicked = selected then exit(selected);
    if board[y,x] <> NONE then begin
        selected := clicked;
        NeoWaitForVblank;
        DrawBalls;
        DrawSelection(x,y);
        exit(NONE);
    end;
    if not FindPath(selected,clicked) then exit(NONE);
    result := clicked;
end;

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////   MAIN
//////////////////////////////////////////////////////////////////////////////

begin
    SetPalette;

    if not NeoLoad(CONFIG_FILE,word(@config)) then begin
        FillByte(config,sizeOf(config),0);
        config.nextMode := 1;
    end;

    if not NeoIsMousePresent then begin
        Writeln('A mouse is required to play this game.');
        Writeln('Please connect the mouse and restart.');
        exit;
    end;

    NeoShowCursor(1);
    ClearBoard;

    repeat
        start := false;                   // TITLE SCREEN
        NeoWaitForVblank;
        Clrscr;
        DrawScreen;
        if ballcount <> 0 then ShowGameOver;

        repeat
            NeoWaitForVblank;
            DrawStart;
            NeoIntRandom($ffff); // just polling RNG to shuffle it more
            ReadInput;
            if mouseButChange and mouseBut = 1 then begin
                // start click
                if (mouseX>0) and (mouseX<80) and (mouseY>160) and (mouseY<200) then start := true;
                // level click
                if (mouseX>0) and (mouseX<80) and (mouseY>62) and (mouseY<84) then ChangeLevel;
                // next mode click
                if (mouseX>0) and (mouseX<80) and (mouseY>92) and (mouseY<104) then ChangeNextMode;
            end;
        until start;

        while KeyPressed do ReadInput; // clear keyboard buffer
        quit := false;
        score := 0;
        prevkey := #0;
        ClearBoard;
        TossNext;
        Clrscr;
        DrawScreen;
        DropRandomBalls(5);

        repeat                             // MAIN GAME LOOP
            FindStrikes;
            NeoWaitForVblank;
            DrawBalls;
            DrawScore;
            DrawNext;
            selected := NONE;
            target := NONE;

            repeat
                ReadInput;
                if IsBoardClicked then selected := SelectBall(boardX,boardY);
            until (selected <> NONE) or quit;

            repeat
                ReadInput;
                if IsBoardClicked then target := SelectTarget(boardX,boardY);
            until (target <> NONE) or quit;

            if not quit and (selected <> target) and FindPath(selected,target) then begin
                MoveBall(selected,target);
                FindStrikes;
                if strikedballs = 0 then PopulateNext;
            end;
        until (ballcount = 81) or quit;

        if score>config.hiscores[config.level] then begin
            config.hiscores[config.level] := score;
            DrawHiScore;
            SaveConfig;
        end;

    until false;
end.